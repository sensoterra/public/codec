
function DepthSaturationMaskMap() {
    // Nothing to do
}

DepthSaturationMaskMap.get = function(key) {
    switch ( key ) {
    case 0:
        return 0x01;
    case 10:
        return 0x01;
    case 20:
        return 0x02;
    case 30:
        return 0x04;
    case 45:
        return 0x08;
    case 60:
        return 0x10;
    case 90:
        return 0x20;
    default:
        return -1;
    }
};

DepthSaturationMaskMap.has = function(key) {
    return (DepthSaturationMaskMap.get(key) !== -1);
};

function toHexString(byte_array) {
    return Array.prototype.map
        .call(
            byte_array,
            function(byte) {
                return ('0' + (byte & 0xFF).toString(16)).slice(-2);
            }
        )
        .join('');
}

function formatString(theString, argumentArray) {
    var regex = /%s/;
    var _r = function(p, c) {
        return p.replace(regex, c);
    };
    return argumentArray.reduce(_r, theString);
}

/**
 * brief Applies IEEE 754 half-precision binary floating-point format. No optimizations.
 */
function encodeFloat16(value, littleEndian) {
    littleEndian = littleEndian || false;
    var result = 0;
    if ( isNaN(value) ) {
        result |= (0x1F << 10);  // exponent
        result |= (1 << 9);      // significant
    }
    else if ( !isFinite(value) || (Math.abs(value) > (Math.pow(2, 16) - Math.pow(2, 5))) ) {
        result = value < 0 ? ((1 << 15) >>> 0) : 0;
        result |= (0x1F << 10);  // exponent
    }
    else if ( Math.abs(value) > Math.pow(2, -14) ) {
        result = value < 0 ? (1 << 15) : 0;
        var exp_bias = 15;
        var max_exp = 15;
        var min_exp = -14;
        var exponent = Math.min(max_exp, Math.max(min_exp, Math.floor(Math.log(Math.abs(value)) / Math.log(2))));
        result |= ((exponent + exp_bias) << 10);
        var signifcant = 0;
        if ( exponent === 0 ) {
            if ( Math.abs(value) > 0 ) {
                signifcant = Math.round((Math.abs(value) / (Math.pow(2, -14))) * (Math.pow(2, 10)));
            }
        }
        else {
            signifcant = Math.round(((Math.abs(value) / (Math.pow(2, exponent))) - 1) * Math.pow(2, 10));
        }
        result |= signifcant;
    }

    if ( littleEndian ) {
        result = (((result & 0xFF) >>> 0) << 8) | ((result >> 8) & 0xFF);
    }

    return result;
}

function encodeCalibration(downlink_obj) {
    var result = {
        data: {},
        warnings: null,
        errors: null,
    };

    if ( Array.isArray(downlink_obj) ) {
        result.data = {
            bytes: downlink_obj,
            hex_str: toHexString(downlink_obj),
        };
        return downlink_obj;
    }

    var depth_bit_mask = 0;
    for ( var index = 0; index < downlink_obj.depths.length; index++ ) {
        var depth = downlink_obj.depths[index];
        if ( depth === 0 && downlink_obj.depths.length > 1 ) {
            if ( !result.errors ) {
                result.errors = [];
            }
            result.errors.push('\'SD\' depth must be provided as the only depth');
            break;
        }
        var depth_num = typeof (depth) === 'string' ? parseInt(depth) : depth;
        if ( !DepthSaturationMaskMap.has(depth_num) ) {
            if ( !result.errors ) {
                result.errors = [];
            }
            result.errors.push(formatString('Invalid depth: %s', [depth_num.toString()]));
            break;
        }

        depth_bit_mask = depth_bit_mask | DepthSaturationMaskMap.get(depth_num);
    }

    if ( !result.errors ) {
        // The format is:
        // 1-byte msg_id
        // 1-byte depth spec
        // 2-bytes a
        // 2-bytes b
        // 2-bytes c
        // 2-bytes sp
        var payload = new Array(10);

        payload[0] = 0x03;
        payload[1] = depth_bit_mask;

        var param_a = 0.0;
        var param_b = 0.0;
        var param_c = 0.0;
        var saturation_point = 0.0;

        if ( Object.prototype.hasOwnProperty.call(downlink_obj, 'a') &&
             Object.prototype.hasOwnProperty.call(downlink_obj, 'b') &&
             Object.prototype.hasOwnProperty.call(downlink_obj, 'c') &&
             Object.prototype.hasOwnProperty.call(downlink_obj, 'sp') ) {
            param_a = downlink_obj.a;
            param_b = downlink_obj.b;
            param_c = downlink_obj.c;
            saturation_point = downlink_obj.sp;
        }
        else if ( Object.prototype.hasOwnProperty.call(downlink_obj, 'a') ||
                  Object.prototype.hasOwnProperty.call(downlink_obj, 'b') ||
                  Object.prototype.hasOwnProperty.call(downlink_obj, 'c') ||
                  Object.prototype.hasOwnProperty.call(downlink_obj, 'sp') ) {
            if ( !result.errors ) {
                result.errors = [];
            }
            result.data = {
                bytes: [],
                hex_str: null,
            };
            result.errors.push('Input calibration parameters object must contain a, b, c and sp properties or none');
        }

        if ( result.errors === null ) {
            var encoded_param_a = encodeFloat16(param_a, true);
            payload[2] = (encoded_param_a & 0xFF00) >> 8;
            payload[3] = encoded_param_a & 0xFF;

            var encoded_param_b = encodeFloat16(param_b, true);
            payload[4] = (encoded_param_b & 0xFF00) >> 8;
            payload[5] = encoded_param_b & 0xFF;

            var encoded_param_c = encodeFloat16(param_c, true);
            payload[6] = (encoded_param_c & 0xFF00) >> 8;
            payload[7] = encoded_param_c & 0xFF;

            var encoded_param_sp = encodeFloat16(saturation_point, true);
            payload[8] = (encoded_param_sp & 0xFF00) >> 8;
            payload[9] = encoded_param_sp & 0xFF;

            result.data = {
                bytes: payload,
                hex_str: toHexString(payload),
            };
        }
    }
    else {
        result.data = {
            bytes: [],
            hex_str: null,
        }
    }

    return result;
}

