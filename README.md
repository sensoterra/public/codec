# Sensoterra Codec

This document describes the payload for Local Mode (probe sends volumetric moisture percentual) measurements uplinks, as well as the Status Report uplinks, which are available for the probes on Local and Cloud Mode (the later, probe shares data with Sensoterra Cloud). It is also depicted the payload for the Soil Calibration downlink for Local Mode.

Important to notice is that Local Mode with volumetric moisture measurement uplinks and soil calibration downlinks are only available for probes with firmare from 1413 and up. Status Report uplinks are only supported for firmware 1415 and up.

For more details about the features availables on both probe modes (Local and Cloud), please contact Sensoterra.

Sensoterra also provides an javascript encoder and decoder. They can be found under `src/`. The encoder and decoder are compatible with ES5 and are not specific for any LoRa provider and platform, and it is most likely necessary to encapsulate the calls of the encoder and decoder functions inside the methods required by your platform.

Below you can see and example of Sensoterra's descoder and encoder on TTN (more details <https://www.thethingsindustries.com/docs/integrations/payload-formatters/javascript/>):

For the custom javascript decoder:

```JavaScript
function decodeUplink(input) {
    return decodePayload(input.fPort, input.bytes /* as a byte array, not a hex string */);
}

// Below place the content of sensoterra-decoder.js
```

```JavaScript
function encodeDownlink(input) {
    return encodeCalibration(input);
}

// Below place the content of sensoterra-encoder.js
```

## Milesight gateways Embedded Network

For usage on Milesight gateways Embedded Network: if is decided to swtich from Local Mode to Cloud mode on this type of LNS, is necessary to provide a Javascript decoder that provides additional information before sending the LNS package towards Sensoterra's Servers. Below there is an example of such decoder:

```Javascript
function Decode(uplink_fport, uplink_payload) {
    var lora_package = {};

    lora_package.dev_eui = LoRaObject.devEUI;
    lora_package.uplink = {
        timestamp: LoRaObject.time,
        fport: uplink_fport,
        fcntup: LoRaObject.fCnt,
        payload: LoRaObject.data,
        gateways: []
    };

    for ( var i = 0; i < LoRaObject.rxInfo.length; i++ ) {
        lora_package.uplink.gateways.push({
            name: LoRaObject.rxInfo[i].mac,
            rssi: LoRaObject.rxInfo[i].rssi,
            snr: LoRaObject.rxInfo[i].LoRaSNR,
            latitude: LoRaObject.rxInfo[i].latitude,
            longitude: LoRaObject.rxInfo[i].longitude,
            altitude: LoRaObject.rxInfo[i].altitude,
        });
    }

    lora_package.uplink.spreading_factor = LoRaObject.txInfo.dataRate.spreadFactor;
    if ( !isNaN(lora_package.uplink.spreading_factor) ) {
        lora_package.uplink.spreading_factor = formatString('SF%s', [lora_package.uplink.spreading_factor]);
    }
    lora_package.uplink.frequency = LoRaObject.txInfo.frequency;

    // In case of using Local Mode and also sending the packages to Sensoterra's servers
    // lora_package.uplink.decoded = decodePayload(uplink_fport, uplink_payload);

    return lora_package;
}
```

# Payload Documentation

## Integer encoding

Integers are stored in little endian. Bits (like a 10 bits integer) can be decoded as follows (example code in Python, needs [bitstream](https://pypi.org/project/bitstream/)):

```python
from bitstring import BitStream
from typing import Any

def hex_to_bitstream(payload: str) -> BitStream:
    """Convert a hex string to a bit stream.
    """
    bs = BitStream("0x" + payload)
    for i in range(len(bs) // 8):
        bs.reverse(i * 8, (i+1) * 8)
    return bs

def extract_uint(bs: BitStream, bits: int) -> Any:
    """Extract bits from a bit stream and convert it to an unsigned int.
    """
    val = bs.read(bits)
    val.reverse()
    return val.uint
```



## Measurements Uplink

A measurement uplink on Local mode consists of:

- Six soil moisture measurements as volumetric water content % (VWC%)
- A flag to indicate that was an overshot over saturation point. This will happen during the process of finding the right calibration, but can also help to see a mismatch with the soil type and the correct calibration.
- The current temperature (for Single Depth probes, the temperature sensor has some limitations and its readings should be used as just an indication. Please contact Sensoterra for more details).

The uplink payload has always 9 bytes length (72 bits) and is sent through fport 11. In case of Multi Depth sensors (consult Sensoterra about the types of sensors), the payload from the previous reading is sent through fport 12.

The payload is organized in the way describe on the following table:

| Position | Size (bits) | Range    | Resolution | Purpose                   |
| -------- | ----------- | -------- | ---------- | ------------------------- |
| 0        | 10          | 0 - 100  | 0.1        | Soil moisture reading 1   |
| 10       | 10          | 0 - 100  | 0.1        | Soil moisture reading 2   |
| 20       | 10          | 0 - 100  | 0.1        | Soil moisture reading 3   |
| 30       | 10          | 0 - 100  | 0.1        | Soil moisture reading 4   |
| 40       | 10          | 0 - 100  | 0.1        | Soil moisture reading 5   |
| 50       | 10          | 0 - 100  | 0.1        | Soil moisture reading 6   |
| 60       | 10          | -30 - 70 | 0.1        | Temperature               |
| 70       | 1           | 0 - 1    | 1          | Saturation point overshot |
| 71       | 1           | 0 - 1    | 1          | Reserved for future use   |

The readings are interpreted as follows:

| Measurement                | Single Depth                   | Multi Depth            |
| -------------------------- | ------------------------------ | ---------------------- |
| 1                          | Current soil moisture (t)      | Soil moisture at 10 cm |
| 2                          | Previous measurement, at t - 1 | Soil moisture at 20 cm |
| 3                          | Measurement at t - 2           | Soil moisture at 30 cm |
| 4                          | Measurement at t - 3           | Soil moisture at 45 cm |
| 5                          | Measurement at t - 4           | Soil moisture at 60 cm |
| 6                          | Measurement at t - 5           | Soil moisture at 90 cm |
| Temperature                | Inside probe housing (head)    | At 25 cm depth         |
| Saturation point overshoot | For current measurement        | For any depth          |

And for decoding:

| Measurement                     |   Payload    | Interpretation                            |
| ------------------------------- | :----------: | ----------------------------------------- |
| Volumetric water content as a % |  0 to 1000   | Moisture times 10                         |
|                                 | 1001 to 1022 | Reserved for future use                   |
|                                 |     1023     | Bad reading                               |
| Calibration mismatch            |      0       | No overshot detected                      |
|                                 |      1       | Overshot detected                         |
| Temperature                     |      0       | Temperature is 30 °C or lower             |
|                                 |  1 to 1020   | Temperature with a 30 °C offset, times 10 |
|                                 |     1021     | Temperature is above 72 °C                |
|                                 |     1022     | Reserved for future use                   |
|                                 |     1023     | Bad reading                               |

As an example, the nine byte uplink `f8e0830f733331111b` received at FPort 11 or 12 should be decoded as:

  * Soil moistures: [24.8, 24.8, 24.8, 46.0, 30.7, 7.6]
  * Temperature: 13.3 °C
  * SP overshoot: no

And when using the JavaScript decoder the result will be:

```Json
{
    "type": "Measurement",
    "data": {
        "readings": [
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 24.8
            },
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 24.8
            },
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 24.8
            },
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 46.0
            },
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 30.7
            },
            {
                "error_flag": false,
                "type": "volumetric_moisture",
                "unit": "%",
                "value": 7.6
            },
            {
                "error_flag": false,
                "type": "temperature",
                "unit": "℃",
                "value": 13.3
            }
        ],
        "saturation_point_overshot": false
    },
    "errors": null,
    "warnings": null
}
```

## Probe Calibration Downlinks

The probes In Local Mode can have their soil calibration set via a downlink. By default, the probe Is set on "Uncalibrated", and will just send a volumetric moisture level between 0 and 100%, regardless the soil.
The calibration consists of 3 arguments (a, b and c) used in the fitting curve programmed in the probe, and the soil saturation point.

The soil calibration downlink payload consists of 10 bytes (80 bits) and must be send on fport 1, organized according to the table below.

| Position | Syze (bytes) | Content       | Purpose                             |
| -------- | ------------ | ------------- | ----------------------------------- |
| 0        | 1            | 0x03          | Message type identification         |
| 1        | 1            | 8-bits mask   | Depth specification                 |
| 2        | 2            | Float 16-bits | Parameter *a*                       |
| 4        | 2            | Float 16bits  | Parameter *b*                       |
| 6        | 2            | Float 16bits  | Parameter *c*                       |
| 8        | 2            | Float 16bits  | Parameter *sp* (saturation point %) |

Float 16 bits it is a half-precision floating-point, having a range ±65,504 with the minimum value above 1 being 1 + 1/1024 and have log10(211) ≈ 3.3 decimal digits.

The depth specification is a bitmask indicating for which depths the calibration corresponds to, according to the following specification:

| Bit | Significance | Contents | For Multi-Depth sensor                                            | For Single Depth                                   |
| --- | ------------ | -------- | ----------------------------------------------------------------- | -------------------------------------------------- |
| 7   | Most         | 0        | Not used                                                          | Not used                                           |
| 6   | -            | 0        | Not used                                                          | Not used                                           |
| 5   |              | 0..1     | 1 if reading exceeds saturation point at depth 90 cm; 0 otherwise | Not used                                           |
| 4   |              | 0..1     | 1 if reading exceeds saturation point at depth 60 cm; 0 otherwise | Not used                                           |
| 3   |              | 0..1     | 1 if reading exceeds saturation point at depth 45 cm; 0 otherwise | Not used                                           |
| 2   |              | 0..1     | 1 if reading exceeds saturation point at depth 30 cm; 0 otherwise | Not used                                           |
| 1   |              | 0..1     | 1 if reading exceeds saturation point at depth 20 cm; 0 otherwise | Not used                                           |
| 0   | Least        | 0..1     | 1 if reading exceeds saturation point at depth 10 cm; 0 otherwise | 1 if reading exceeds saturation point; 0 otherwise |

Depths 2 to 6 must be set to 0 for Single Depth probes. The parameters are only applied to depths set to 1. In case all depths are set to 0, the probe does switch to Local Mode, but the parameters are ignored. In this last case, the probe for all applicable depths uses the Uncalibrated configuration.

An exempla for the JSON provided to the sensosterra encoder:

```Json
{
    "a": 4571.187333,
    "b": -6.16333333,
    "c": 0.0,
    "sp": 40.0,
    "depths": [0]
}
```

In the table below you can find a few examples of possible soil calibrations:

| Soil type            | *a*       | *b*     | *c*   | *sp* | Payload              |
| -------------------- | --------- | ------- | ----- | ---- | -------------------- |
| Sand 1.2 (0.0% org.) | 4571.1873 | -6.1633 | 0.0   | 40.0 | 0301776c2ac600000051 |
| Sand 4 (12.0% org.)  | 373.317   | -4.89   | 0.0   | 52.0 | 0301d55de4c400008052 |
| Coarse tree soil     | 1587.41   | -5.84   | 3.509 | 61.0 | 03013366d7c50543a053 |
| Sandy Clay Loam 1    | 881.000   | -3.617  | 6.020 | 56.5 | 0301e2623cc305461053 |

## Probe KPI Reports Uplinks

These report uplinks are meant to retrieve important data from the probe, over its communication with the network, battery level, uptime, and other parameters that are useful to check on the probe's behavior.

The KPI reports are only sent from the probe in response to a specific downlink command, with only 1 exception: the KPI Report 1 is sent every 24 hours (and every time the probe resets) for probes on Local Mode.

To request a KPI report, schedule the following 2-byte downlink payload over FPort 1:

| Position | Size (bytes) | Contents   | Purpose                 |
| -------- | ------------ | ---------- | ----------------------- |
| 0        | 1            | 0x05       | Message type identifier |
| 1        | 1            | 0x01..0x0C | Report type ID          |

The following table list of Downlinks and the corresponding KPI Report uplink sent in response:

| Downlink Payload | KPI Report Uplink | Uplink FPort | Probe Model            |
| ---------------- | ----------------- | ------------ | ---------------------- |
| 0x0501           | KPI Report 1      | 100          | Single and Multi depth |
| 0x0502           | KPI Report 2      | 101          | Single and Multi depth |
| 0x0503           | KPI Report 3      | 102          | Single and Multi depth |
| 0x0504           | KPI Report 4      | 103          | Single and Multi depth |
| 0x0505           | KPI Report 5      | 104          | Single and Multi depth |
| 0x0506           | KPI Report 6      | 105          | Single and Multi depth |
| 0x0507           | KPI Report 7      | 106          | Single and Multi depth |
| 0x0508           | KPI Report 8      | 107          | Multi depth only       |
| 0x0509           | KPI Report 9      | 108          | Multi depth only       |
| 0x050A           | KPI Report 10     | 109          | Multi depth only       |
| 0x050B           | KPI Report 11     | 110          | Multi depth only       |
| 0x050C           | KPI Report 12     | 111          | Multi depth only       |

### KPI Report 1

The KPI Report 1 uplink has a 9-byte long payload containing the information in the following table.

This Report is also send automatically every 24 hours for probes on Local Mode.

| Position | Size (bytes) | Contents                        |
| -------- | ------------ | ------------------------------- |
| 0        | 1            | Battery voltage                 |
| 1        | 1            | Estimated Signal Power          |
| 2        | 1            | Saturation point overshoot      |
| 3        | 2            | Firmware revision               |
| 5        | 1            | Last reset (context and reason) |
| 6        | 3            | Uptime                          |

#### Battery

Probe battery voltage payload should be decoded as follows:

- 0: reserved
- 1..254: `payload / 125 + 1.7` Volt
- 255: probe was not able to measure battery voltage

For example, a reading of 162 should be decoded as 2996 mV.

#### Estimated Signal Power

Estimated Signal Power (ESP) is the received signal strength of the gateway by the probe, measured in dBm.

Higher is a better value. Here is how you could classify the values by quality of signal:

- lower than -135 dBm is weak
- between -135 and -125 is average
- above -125 is good

Payload is an unsigned byte, representing a value between -200 and 30 dBm. The payload can be decoded as follows:

payload = 0, if esp < -220
payload = ESP + 220
payload = 250, if esp > 30

Examples:

| Payload |  ESP  |
| :-----: | :---: |
|   103   | -117  |
|   222   |   2   |

#### Saturation point overshoot

The saturation point (SP) overshoot is a bitmask indicating for which depths on overshoot occurred:

| Bit | Significance | Content | For Single Depth                                   | For Multi Depth                                             |
| --- | ------------ | ------- | -------------------------------------------------- | ----------------------------------------------------------- |
| 7   | Most         | 0       | Not used                                           | Not used                                                    |
| 6   | -            | 0..1    | 0                                                  | 1                                                           |
| 5   |              | 0..1    | Not used                                           | 1 if reading exceeds saturation point at 90 cm; 0 otherwise |
| 4   |              | 0..1    | Not used                                           | 1 if reading exceeds saturation point at 60 cm; 0 otherwise |
| 3   |              | 0..1    | Not used                                           | 1 if reading exceeds saturation point at 45 cm; 0 otherwise |
| 2   |              | 0..1    | Not used                                           | 1 if reading exceeds saturation point at 30 cm; 0 otherwise |
| 1   |              | 0..1    | Not used                                           | 1 if reading exceeds saturation point at 20 cm; 0 otherwise |
| 0   | Least        | 0..1    | 1 if reading exceeds saturation point; 0 otherwise | 1 if reading exceeds saturation point at 10 cm; 0 otherwise |

Where Sensor type is:

- 0 if Single depth sensor
- 1 if Multi depth sensor

Depths 2 to 6 will be `0` in case of a Single Depth probe. Examples:

- Bitmask `00000001` for a SD probe: SP overshoot occurred
- Bitmask `10100010` for a MD probe: SP overshoot occurred at depths 6 (90 cm) and 2 (20 cm)

The Overshoot flag is reset when a new calibration is set for the probe.

#### Firmware

Firmware revision is encoded as a two byte unsigned integer. According to the probe's generation, the firmware can be interpreted as follows:

For generation 2.5:

| Position (bits) | 15 (most significant) |      14-0       |
| --------------- | :-------------------: | :-------------: |
| Value           |           0           | Revision number |

For generation 3:

| Position (bits) | 15 (most significant) |     14-10     |      9-5      |      4-0      |
| --------------- | :-------------------: | :-----------: | :-----------: | :-----------: |
| Value           |           1           | Major version | Minor version | Patch version |

#### Last reset

The 8 bits of the last reset (reset) field contain:

| Position |   7-4   |  3-0   |
| -------- | :-----: | :----: |
| Value    | Context | Reason |

Most common reasons of a reset:

| Value | Meaning   | Description                                                                |
| ----- | --------- | -------------------------------------------------------------------------- |
| 0     | Power on  | Set when power is initially applied.                                       |
| 1     | Pin reset | Set when a reset is triggered by the hardware pin on the board.            |
| 2     | Brown out | Triggered when the voltage dropped below the low voltage detect threshold. |
| 3     | Software  | Set during software reset.                                                 |
| 4     | Watchdog  | Set when a running watchdog timer fails to be refreshed.                   |

Possible contexts:

| Value | State                      |
| ----- | -------------------------- |
| 0     | In (deep) sleep            |
| 1     | Soil measurement           |
| 2     | Temperature measurement    |
| 3     | Joining network            |
| 4     | Sending confirmed uplink   |
| 5     | Sending unconfirmed uplink |
| 6     | Receiving downlink         |
| 7     | Applying ADR               |
| 15    | Other                      |

#### Uptime

The uptime is a 3 byte unsigned integer and holds the number of minutes since the last probe reset.

### KPI Report 2

The KPI Report 2 has a 9-bytes legnth payload, with the following content:

| Position | Size (bytes) | Contents                |
| -------- | ------------ | ----------------------- |
| 0        | 1            | Status                  |
| 1        | 2            | Sleep interval          |
| 3        | 1            | Network check interval  |
| 4        | 1            | Network check threshold |
| 5        | 1            | nbTrans                 |

#### Status

The single byte status payload should be interpreted as follows:

| Bit | Use                                              |
| --- | ------------------------------------------------ |
| 7   | Not used                                         |
| 6   | Not used                                         |
| 5   | Not used                                         |
| 4   | Not used                                         |
| 3   | History                                          |
| 2   | Standalone mode                                  |
| 1   | Watchdog enabled                                 |
| 0   | Set for confirmed uplinks, unset for link checks |

#### Sleep interval

Sleep interval is a 2 byte unsigned integer and holds the sleep interval time in minutes.

### KPI Report 3

The KPI Report 3 has a 9-bytes long payload, with the following content:

| Position | Size (bytes) | Contents     |
| -------- | ------------ | ------------ |
| 0        | 9            | Channel mask |

The payload contains the 64 + 8 bit channel mask.

### KPI Report 4

The KPI Report 4 has a 9-bytes long payload, with the following content:

| Position | Size (bytes) | Type         | Contents                                 |
| -------- | ------------ | ------------ | ---------------------------------------- |
| 0        | 3            | Unsigned int | Total time on air in seconds             |
| 4        | 2            | Unsigned int | Total number of resets                   |
| 6        | 2            | Unsigned int | Total number of failed join sessions     |
| 8        | 2            | Unsigned int | Total number of successful join sessions |

Max unsigned int (0xffff or 0xffffff) indicates payload overflow.

### KPI Report 5

The KPI Report 5 has a 9-bytes long payload, with the following content:

| Position | Size (bytes) | Type         | Contents                                   |
| -------- | ------------ | ------------ | ------------------------------------------ |
| 0        | 3            | Unsigned int | Total number of uplinks                    |
| 4        | 2            | Unsigned int | Total number of uplinks SP12 divided by 10 |
| 6        | 2            | Unsigned int | Total number of uplinks SP11 divided by 10 |
| 8        | 2            | Unsigned int | Total number of uplinks SP10 divided by 10 |

Max unsigned int (0xffff or 0xffffff) indicates payload overflow.

### KPI Report 6

The KPI Report 6 has a 5-bytes long payload, with the following content:

| Position | Size (bytes) | Type         | Contents              |
| -------- | ------------ | ------------ | --------------------- |
| 0        | 4            | Unsigned int | Identification number |
| 5        | 1            | Unsigned int | Product number        |

#### Serial number

The serial number is encoded as two separated parts, and the probe serial number is composed by concatenation of product and identification number.

### KPI Report 7 to 12

The KPI Reports from 7 to 12 have the same structure and which is a 9-bytes long payload containing the soil calibration. More details on the following table.

| Report ID | Single depth probe     | Multi depth probe                     |
| --------- | ---------------------- | ------------------------------------- |
| 0x07      | Calibration parameters | Calibration parameters at depth 10 cm |
| 0x08      | Not used               | Calibration parameters at depth 20 cm |
| 0x09      | Not used               | Calibration parameters at depth 30 cm |
| 0x0A      | Not used               | Calibration parameters at depth 45 cm |
| 0x0B      | Not used               | Calibration parameters at depth 60 cm |
| 0x0C      | Not used               | Calibration parameters at depth 90 cm |

The 8 byte payload should be interpreted as follows:

| Position | Size (bytes) | Contents | Purpose        |
| -------- | ------------ | -------- | -------------- |
| 0        | 2            | Float16  | Parameter `a`  |
| 2        | 2            | Float16  | Parameter `b`  |
| 4        | 2            | Float16  | Parameter `c`  |
| 6        | 2            | Float16  | Parameter `sp` |

*NOTE*: The calibration parameters uplink is automatically sent from the probe when a new calibration is received via downlink.
In case of a Multi Depth probe, if more than one depth is configured within the same soil calibration downlink, only one calibration uplink is sent, and its corresponds to the calibration for the deepest point.
