import {DepthSaturationMaskMap, encodeCalibration} from '../src/sensoterra-encoder.js';

export const ERROR_VALUE = 1023;

export const TEMPERATURE_ENCODE_OFFSET = 300;

export const LOW_TEMPERATURE_THRESHOLD = -30.0;

export const HIGH_TEMPERATURE_THRESHOLD = 70.0;

export class ReadingStatus {
    static OK = new ReadingStatus('OK');
    static NOK = new ReadingStatus('ERROR');

    constructor(name) {
        this.name = name;
    }

    toString() {
        return `${this.name}`;
    }
}

export class ReadingType {
    static MOISTURE_PERCENTAGE = new ReadingType('volumetric_moisture');
    static TEMPERATURE_CELSIUS = new ReadingType('temperature');

    constructor(name) {
        this.name = name;
    }

    toString() {
        return `${this.name}`;
    }
}

export class ReadingUnit {
    static PERCENTAGE = new ReadingUnit('%');
    static DEGREES_CELSIUS = new ReadingUnit('℃');

    constructor(name) {
        this.name = name;
    }

    toString() {
        return `${this.name}`;
    }
}

export class Reading {
    constructor({value = ERROR_VALUE, status = ReadingStatus.OK, type = ReadingType.MOISTURE_PERCENTAGE}) {
        this.status = status;
        this.type = type;
        this.value = value;
    }

    toString() {
        return `Reading(status=${this.status}, type=${this.type}, value=${this.value})`;
    }
}

export const LinkCheckMode = {
    LINK_CHECKS: false,
    CONFIRMED_UPLINKS: true,
};

export const ProbeMode = {
    LOCAL_MODE: true,
    CLOUD_MODE: false
};

export function measurementUplinkPayloadGenerator({
    readings_list,
    saturation_point = 100.0,
    nof_bytes = 9,
    lo_temp_threshold = LOW_TEMPERATURE_THRESHOLD,
    hi_temp_threshold = HIGH_TEMPERATURE_THRESHOLD
}) {
    const payload_buffer = new Uint8Array(nof_bytes);
    const payload = new DataView(payload_buffer.buffer);

    const bit_mask = 0xff;

    let bit_mask_shift = 0;
    let byte_index = 0;
    let reading_index = 0;
    let saturation_point_overshot = false

    while ( reading_index < readings_list.length ) {
        let encoded_reading = 0.0;
        const reading = readings_list[reading_index];
        if ( Object.prototype.hasOwnProperty.call(reading, 'status') && reading.status == ReadingStatus.NOK ) {
            encoded_reading = ERROR_VALUE;
        }
        else if ( Object.prototype.hasOwnProperty.call(reading, 'status') &&
                  Object.prototype.hasOwnProperty.call(reading, 'value') ) {
            if ( reading.status === ReadingStatus.NOK ) {
                encoded_reading = ERROR_VALUE;
            }
            else {
                if ( Object.prototype.hasOwnProperty.call(reading, 'type') ) {
                    if ( reading.type === ReadingType.TEMPERATURE_CELSIUS ) {
                        if ( reading.value < lo_temp_threshold ) {
                            encoded_reading = 0;
                        }
                        else if ( reading.value > hi_temp_threshold ) {
                            encoded_reading = 1021;
                        }
                        else {
                            encoded_reading = Math.round(reading.value * 10) + TEMPERATURE_ENCODE_OFFSET;
                        }
                    }
                    else if ( reading.type === ReadingType.MOISTURE_PERCENTAGE ) {
                        saturation_point_overshot = (reading.value > saturation_point);
                        if ( saturation_point_overshot ) {
                            reading.value = saturation_point;
                        }
                        encoded_reading = Math.round(reading.value * 10);
                    }
                }
            }
        }
        else if ( Object.prototype.hasOwnProperty.call(reading, 'value') ) {
            saturation_point_overshot = (reading.value > saturation_point);
            if ( saturation_point_overshot ) {
                reading.value = saturation_point;
            }
            encoded_reading = Math.round(reading.value * 10);
        }

        const first_byte =
            payload.getUint8(byte_index) | ((encoded_reading & (bit_mask >>> bit_mask_shift)) << bit_mask_shift);
        const second_byte_mask = (bit_mask >>> (8 - bit_mask_shift - 2)) << (8 - bit_mask_shift);
        const second_byte = (encoded_reading & second_byte_mask) >> (8 - bit_mask_shift);
        payload.setUint8(byte_index, first_byte);
        payload.setUint8(byte_index + 1, second_byte);

        bit_mask_shift += 2;
        byte_index++;
        reading_index++;

        if ( bit_mask_shift >= 8 ) {
            bit_mask_shift = 0;
            byte_index++;
        }
    }

    if ( saturation_point_overshot ) {
        payload.setUint8(nof_bytes - 1, payload.getUint8(nof_bytes - 1) | 0x40);
    }

    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI1UplinkGenerator({
    battery_voltage_V,
    estimated_signal_power_dBm,
    is_single_depth = true,
    saturation_point_overshot_depths = [],
    firmware_version,
    last_reset_reason,
    last_reset_context,
    uptime_sec,
    nof_bytes = 9,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);

    if ( nof_bytes === 9 ) {
        payload.setUint8(0, Math.round((battery_voltage_V - 1.7) * 125));
        payload.setUint8(1, Math.round(estimated_signal_power_dBm + 220));

        let saturation_mask = is_single_depth ? 0 : 0x80;
        for ( const depth of saturation_point_overshot_depths ) {
            saturation_mask |= DepthSaturationMaskMap.get(depth);
        }
        payload.setUint8(2, saturation_mask);

        let firmware_revision = 0;
        const is_revision_number = /^[0-9]+$/.test(firmware_version);
        if ( is_revision_number ) {
            firmware_revision = parseInt(firmware_version);
            payload.setUint16(3, firmware_revision, true);
        }
        else {
            let revision_parts = firmware_version.split('.');
            if ( revision_parts.length == 3 ) {
                firmware_revision = (parseInt(revision_parts[0]) >>> 0) << 10;
                firmware_revision |= (parseInt(revision_parts[1]) >>> 0) << 5;
                firmware_revision |= (parseInt(revision_parts[2]) >>> 0) << 0;
                firmware_revision |= (1 << 15);
                payload.setUint16(3, firmware_revision, true);
            }
        }

        payload.setUint8(5, ((last_reset_context >>> 0) << 4) | (last_reset_reason >>> 0));

        const uptime_buf = new ArrayBuffer(4);
        const uptime_fuf_view = new DataView(uptime_buf);
        uptime_fuf_view.setUint32(0, uptime_sec, true);

        for ( let i = 0; i < 3; i++ ) {
            payload.setUint8(6 + i, uptime_fuf_view.getUint8(i));
        }
    }

    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI2UplinkGenerator({
    link_check_mode = LinkCheckMode.CONFIRMED_UPLINKS,
    watchdog_enabled = true,
    probe_mode = ProbeMode.LOCAL_MODE,
    history_enabled = false,
    sleep_interval_min = 60,
    network_check_counter_interval = 8,
    network_check_counter_threshold = 3,
    number_retransmissions = 0,
    nof_bytes = 6,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);

    if ( nof_bytes === 6 ) {
        payload.setUint8(0, link_check_mode ? 1 : 0);
        payload.setUint8(0, payload.getUint8(0) | (watchdog_enabled ? (1 << 1) : 0));
        payload.setUint8(0, payload.getUint8(0) | (probe_mode ? (1 << 2) : 0));
        payload.setUint8(0, payload.getUint8(0) | (history_enabled ? (1 << 3) : 0));

        payload.setUint16(1, sleep_interval_min, true);
        payload.setUint8(3, network_check_counter_interval);
        payload.setUint8(4, network_check_counter_threshold);
        payload.setUint8(5, number_retransmissions);
    }

    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI3UplinkGenerator({
    byte_array,
    nof_bytes = 9,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);

    for ( let i = 0; i < nof_bytes; i++ ) {
        payload.setUint8(i, byte_array[i]);
    }
    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI4UplinkGenerator({
    total_time_on_air_s,
    total_resets,
    total_successful_join_sessions,
    total_failed_join_sessions,
    nof_bytes = 9,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);
    if ( nof_bytes === 9 ) {
        payload.setUint32(0, Math.min(0xFFFFFF, total_time_on_air_s), true);
        // Since the total time on air should fit in 3 bytes, the 4th byte is 0 and is used to store the next field.
        payload.setUint16(3, Math.min(0xFFFF, total_resets), true);
        payload.setUint16(5, Math.min(0xFFFF, total_successful_join_sessions), true);
        payload.setUint16(7, Math.min(0xFFFF, total_failed_join_sessions), true);
    }
    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI5UplinkGenerator({
    total_uplinks,
    total_uplinks_sf12,
    total_uplinks_sf11,
    total_uplinks_sf10,
    nof_bytes = 9,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);
    if ( nof_bytes === 9 ) {
        payload.setUint32(0, Math.min(0xFFFFFF, total_uplinks), true);
        // Since the total time on air should fit in 3 bytes, the 4th byte is 0 and is used to store the next field.
        payload.setUint16(3, Math.min(0xFFFF, Math.ceil(total_uplinks_sf12 / 10.0)), true);
        payload.setUint16(5, Math.min(0xFFFF, Math.ceil(total_uplinks_sf11 / 10.0)), true);
        payload.setUint16(7, Math.min(0xFFFF, Math.ceil(total_uplinks_sf10 / 10.0)), true);
    }
    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function reportKPI6UplinkGenerator({
    serial_number,
    nof_bytes = 5,
}) {
    const payload_buffer = new ArrayBuffer(nof_bytes);
    const payload = new DataView(payload_buffer);
    if ( nof_bytes === 5 ) {
        const sn_regex = /^([1-9]+)[0]+([0-9]+)$/;
        const sn_match = serial_number.match(sn_regex);
        if ( sn_match ) {
            const id_number = parseInt(sn_match[2]);
            payload.setUint32(0, id_number, true);
            const product_number = parseInt(sn_match[1]);
            payload.setUint8(4, product_number);
        }
    }
    return {
        bytes: [...new Uint8Array(payload.buffer)],
        hex_str: [...new Uint8Array(payload.buffer)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}

export function generateReportCalibration({
    depth = 0,
    a,
    b,
    c,
    sp,
    nof_bytes = 9,
}) {
    let payload = encodeCalibration({
        a,
        b,
        c,
        sp,
        depths: [depth],
    });
    payload.data.bytes.shift();

    if ( nof_bytes > 9 ) {
        for ( let i = 0; i < 9 - nof_bytes; i++ ) {
            payload.data.bytes.pop();
        }
    }

    payload.data.bytes[0] |= (depth === 0 ? 0 : (1 << 7));

    return {
        bytes: [...new Uint8Array(payload.data.bytes)],
        hex_str: [...new Uint8Array(payload.data.bytes)].map((x) => x.toString(16).padStart(2, '0')).join(''),
    };
}